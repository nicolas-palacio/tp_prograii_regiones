package controlador;

import java.util.ArrayList;
import java.util.HashMap;
import logica.ArbolGeneradorMinimo;
import logica.Grafo;


public class Controlador {
	private static HashMap<ArrayList<Integer>, Integer> aristasRet;
	private static ArbolGeneradorMinimo arbol;
	private static Grafo grafo;
				
	public static HashMap<ArrayList<Integer>, Integer> generarAGM(int cantVertices,
			HashMap<ArrayList<Integer>, Integer> aristas) {
		aristasRet=new HashMap<>();

		grafo= generarAristas(aristas,cantVertices);	
		arbol=new ArbolGeneradorMinimo(grafo,grafo.cantidadVertices());
		borrarAristasInexistentes(arbol,grafo);
		
		return aristasRet;
	}
		
	private static Grafo generarAristas(HashMap<ArrayList<Integer>,Integer> verticesVecinosConPesos,
			int cantidadVertices) {
		Grafo ret= new Grafo(cantidadVertices);		
		
		verticesVecinosConPesos.forEach(
				(clave,valor)->agregarAristaGrafo(ret,clave,valor)
							
				);
					
		return ret;
	}
	
	private static void agregarAristaGrafo(Grafo grafo,ArrayList<Integer> vertices ,Integer peso) {
		
		int verticeOrigen=vertices.get(1);
		int verticeDestino=vertices.get(0);
		
		grafo.agregarArista(verticeOrigen,verticeDestino,peso);		
	}
			
	private static void borrarAristasInexistentes(ArbolGeneradorMinimo arbol,Grafo grafo) {
		aristasRet.clear();
		
		 int i=0, j=0;
	        while (i < arbol.getA().length) {
	  
	            if (j >= arbol.getA()[0].length) {
	                i++;
	                j = 0;
	            } else if(arbol.getA()[i][j]==false && grafo.getA()[i][j]) {
	            	ArrayList<Integer>verticesVecinos=new ArrayList<>();
	            	verticesVecinos.add(i);
	            	verticesVecinos.add(j);
	            	aristasRet.put(verticesVecinos, i);

	            	j++;
	            }else {
	                j++;
	            }	 
	        }	
	}
	
	public static HashMap<ArrayList<Integer>, Integer> generarRegiones(int cantidadRegiones) {
		arbol.generarRegiones(cantidadRegiones);
		borrarAristasInexistentes(arbol,grafo);
		
		return aristasRet;
		
	}
}