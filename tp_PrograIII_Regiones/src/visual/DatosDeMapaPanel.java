package visual;

import javax.swing.JPanel;
import javax.swing.JLabel;
import java.awt.Font;
import java.util.ArrayList;
import java.util.HashMap;

import javax.swing.JTextField;

import org.openstreetmap.gui.jmapviewer.Coordinate;
import org.openstreetmap.gui.jmapviewer.interfaces.MapMarker;
import javax.swing.border.CompoundBorder;

@SuppressWarnings("serial")
public class DatosDeMapaPanel extends JPanel {
	private ArrayList<JLabel> lblAristas;
	private ArrayList<JTextField> textFieldLista;
	private HashMap<ArrayList<Coordinate>,Integer> aristasConPesos;
	private HashMap<ArrayList<Coordinate>,Integer> coordenadasTextFieldIndex;
	private HashMap<Coordinate,Integer> coordenadaVertice;
	private HashMap<ArrayList<Integer>,Integer> verticesConAristas;
	private int txtFieldIndex;
	
	/**
	 * Create the panel.
	 */
	public DatosDeMapaPanel() {
		setBorder(new CompoundBorder());
		this.setBounds(450, 250, 440, 200);
		this.lblAristas=new ArrayList<>();
		this.aristasConPesos=new HashMap<>();
		this.coordenadasTextFieldIndex=new HashMap<>();
		this.textFieldLista=new ArrayList<>();	
		this.coordenadaVertice=new HashMap<>();
		this.verticesConAristas=new HashMap<>();
		this.txtFieldIndex=0;
		setLayout(null);																							
	}
	
	public void agregarLblArista(String provinciaOrigen,String provinciaDestino,Coordinate origen,Coordinate destino){
		JLabel lblArista = new JLabel(provinciaOrigen+"--"+provinciaDestino);
		lblArista.setFont(new Font("Tahoma", Font.PLAIN, 18));
		JTextField textField = new JTextField();
				
		if(this.lblAristas.isEmpty()) {
			lblArista.setBounds(28, 46, 300, 22);
			textField.setBounds(362, 52, 66, 20);
			
			this.lblAristas.add(lblArista);
			this.textFieldLista.add(textField);
			agregarCoordenadas(origen,destino);
			this.txtFieldIndex++;			
		}else {
			lblArista.setBounds(28, lblAristas.get(lblAristas.size()-1).getY()+22, 300, 20);
			textField.setBounds(362, textFieldLista.get(textFieldLista.size()-1).getY()+22, 66, 20);
			this.lblAristas.add(lblArista);
			this.textFieldLista.add(textField);
			agregarCoordenadas(origen,destino);			
			this.txtFieldIndex++;
		}
		
		add(textField);
		add(lblArista);

		this.revalidate();
		this.repaint();
	}
	
	private void agregarCoordenadas(Coordinate origen, Coordinate destino) {
		ArrayList<Coordinate> coordenadas=new ArrayList<>();
		
		coordenadas.add(origen);
		coordenadas.add(destino);
		if(!this.aristasConPesos.containsKey(coordenadas)) {
			this.coordenadasTextFieldIndex.put(coordenadas, this.txtFieldIndex);
			this.aristasConPesos.put(coordenadas, 0);
		}						
	}
	
	public void generarPesos(HashMap<Coordinate,ArrayList<Coordinate>> verticesVecinos) {
					
		this.aristasConPesos.forEach(
				(clave,valor)-> generarPesoVertice(clave.get(0),clave.get(1))
				);
		
	}
	
	private void generarPesoVertice(Coordinate origen,Coordinate destino) {
		
		ArrayList<Coordinate> coordenadas=new ArrayList<>();
		coordenadas.add(origen);
		coordenadas.add(destino);
	
		int indiceAsociado=this.coordenadasTextFieldIndex.get(coordenadas);
		
		int pesoArista=Integer.parseInt(this.textFieldLista.get(indiceAsociado).getText());
		this.aristasConPesos.put(coordenadas,pesoArista);
				
	}
	
	public void limpiar() {
		this.coordenadasTextFieldIndex.clear();
		this.aristasConPesos.clear();
		this.textFieldLista.clear();
		this.lblAristas.clear();
		this.txtFieldIndex=0;
		this.coordenadaVertice.clear();
		this.verticesConAristas.clear();
		this.removeAll();
		this.repaint();
	}
	
	public void generarCoordenadaVertice(ArrayList<MapMarker>coordenadas ) {	
		
		for(int i=0;i<coordenadas.size();i++) {
			if(!this.coordenadaVertice.containsKey(coordenadas.get(i).getCoordinate())) {			
				this.coordenadaVertice.put(coordenadas.get(i).getCoordinate(),i);
			}
		}
		
		generarAristas();
	}
	
	private void generarAristas() {
		this.aristasConPesos.forEach(
				(clave,valor)->generarArista(clave,valor)
							
				);						
	}
	
	private void generarArista(ArrayList<Coordinate> coordenadas, int peso) {
		int verticeOrigen=dameVerticeIndex(coordenadas.get(1));
		int verticeDestino=dameVerticeIndex(coordenadas.get(0));
		ArrayList<Integer> vertices=new ArrayList<>();
		vertices.add(verticeOrigen);
		vertices.add(verticeDestino);
		
		this.verticesConAristas.put(vertices,peso);
	}
	
	private  int dameVerticeIndex(Coordinate coordenada) {
		
		return this.coordenadaVertice.get(coordenada);		
	}
	
	public Coordinate dameCoordenadaIndex(int index) {
		Coordinate coordenadaOrigen=new Coordinate(0,0);
		coordenadaVertice.forEach(
				(clave,valor)->{
					if(valor==index) {
						coordenadaOrigen.setLat(clave.getLat());
						coordenadaOrigen.setLon(clave.getLon());						
					}
						}
				);
		
		return coordenadaOrigen;		
	}
	
	public HashMap<ArrayList<Coordinate>, Integer> getVerticesVecinosConPesos() {
		return aristasConPesos;
	}
		
	public ArrayList<JTextField> getTextFieldLista() {
		return textFieldLista;
	}

	public HashMap<ArrayList<Integer>, Integer> getVerticesConAristas() {
		return verticesConAristas;
	}
		
}