package visual;


import java.awt.Color;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.HashMap;

import javax.swing.JOptionPane;
import javax.swing.JPanel;

import org.openstreetmap.gui.jmapviewer.Coordinate;
import org.openstreetmap.gui.jmapviewer.JMapViewer;
import org.openstreetmap.gui.jmapviewer.MapMarkerDot;
import org.openstreetmap.gui.jmapviewer.MapPolygonImpl;
import org.openstreetmap.gui.jmapviewer.interfaces.MapMarker;

import logica.Provincia;
import logica.ProvinciaJSON;


@SuppressWarnings("serial")
public class MapaPanel extends JPanel {
	private JMapViewer mapa;
	private DatosDeMapaPanel datosPanel;
	private ArrayList<Coordinate> coordenadas;
	private ArrayList<MapMarker> marcadores;
	private MapMarker markerActual;
	private HashMap<Coordinate,ArrayList<Coordinate>> verticesVecinos;
	private ArrayList<MapPolygonImpl> poligonos;
	private ArrayList<MapPolygonImpl> poligonosEliminados;
	private HashMap<ArrayList<Coordinate>,Integer> coordenadasPoligonoIndex;
	private String provinciaOrigen,provinciaDestino;
	private boolean hayProvinciaSeleccionada;
	
	/**
	 * Create the panel.
	 */
	public MapaPanel() {
		this.setBounds(250, 100, 900, 900);
		this.coordenadas=new ArrayList<>(); 	
		this.datosPanel=new DatosDeMapaPanel();
		datosPanel.setSize(440, 755);
		datosPanel.setLocation(450, 11);
		
		
		this.marcadores=new ArrayList<>();
		this.verticesVecinos= new HashMap<>();
		
		this.mapa=new JMapViewer();
		this.hayProvinciaSeleccionada=false;
		this.poligonos= new ArrayList<>();
		this.poligonosEliminados=new ArrayList<>();
		this.coordenadasPoligonoIndex=new HashMap<>();
		
		this.mapa.setBounds(10, 55, 430, 834);
		Coordinate coordenada=new Coordinate(-38.416097, -63.616672);
		this.mapa.setDisplayPosition(coordenada, 5);
			
		this.mapa.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				agregarPunto((Coordinate)mapa.getPosition(e.getPoint()));
			}
		});
		
		setLayout(null);	
		this.add(datosPanel);
		this.add(mapa);
	}
	
	private void actualizar() {	
		
		actualizarMarcadores();	
		actualizarPoligonos();			
	}

	private void actualizarPoligonos() {
		for(MapPolygonImpl poligono:this.poligonos) {
			if(!this.poligonosEliminados.contains(poligono)) {
				
				this.mapa.addMapPolygon(poligono);
			}
		}
			
	}

	private void actualizarMarcadores() {
		for(int i=0;i<this.marcadores.size();i++) {
			
			if(verticesIguales(this.marcadores.get(i),this.markerActual) && this.hayProvinciaSeleccionada) {
				this.marcadores.get(i).getStyle().setBackColor(Color.red);
			}else {				
				this.marcadores.get(i).getStyle().setBackColor(Color.yellow);
			}
			
			this.mapa.addMapMarker(this.marcadores.get(i));
		}
	}
	
	private void agregarPunto(Coordinate punto) {		
		ProvinciaJSON provinciaJSON=new ProvinciaJSON();
		ArrayList<Provincia>provincias=provinciaJSON.leerJSON("Provincias.JSON").getProvincias();
		
		Coordinate coordenada=new Coordinate(0,0);
		String provincia="";				
	
		for(int i=0;i<provincias.size();i++) {
			Provincia provinciaActual=provincias.get(i);
		
			if((esProvinciaValida(punto, provinciaActual) && this.verticesVecinos.isEmpty())) {
				
				coordenada.setLat(provinciaActual.getCoordenadaPrincipalLat());
				coordenada.setLon(provinciaActual.getCoordenadaPrincipalLon());
				
				this.markerActual=new MapMarkerDot(provincia,coordenada);

				this.provinciaOrigen=provinciaActual.getNombre();
				markerActual.getStyle().setColor(Color.red);
				markerActual.getStyle().setBackColor(Color.red);
								
				this.marcadores.add(markerActual);
				this.coordenadas.add(coordenada);
				this.coordenadas.add(coordenada);
				this.hayProvinciaSeleccionada=true;
				this.verticesVecinos.put(markerActual.getCoordinate(), null);
				
				mapa.addMapMarker(markerActual);
			}
			else if(esProvinciaValida(punto, provinciaActual)) {
				
						if(this.hayProvinciaSeleccionada) {							
																					
							coordenada.setLat(provinciaActual.getCoordenadaPrincipalLat());
							coordenada.setLon(provinciaActual.getCoordenadaPrincipalLon());
													
							MapMarker markerNew= new MapMarkerDot(provincia,coordenada);
							markerNew.getStyle().setColor(Color.red);
							
							
							if(!contieneVecino(markerActual,markerNew) && !verticesIguales(markerActual,markerNew)){
								
								this.hayProvinciaSeleccionada=false;
								
								if(!this.coordenadas.contains(markerNew.getCoordinate())) {
									
									this.marcadores.add(markerNew);
								}
																
								this.coordenadas.add(coordenada);
								this.coordenadas.add(coordenada);
								
								this.provinciaDestino=provinciaActual.getNombre();
								agregarVecino(markerActual,markerNew);
								agregarVecino(markerNew,markerActual);
								
								mapa.addMapMarker(markerNew);
								
								this.markerActual.getStyle().setBackColor(Color.yellow);
						
								dibujarArista(markerNew,this.provinciaOrigen,this.provinciaDestino,markerActual);
							}
																									
						}else {
							coordenada.setLat(provinciaActual.getCoordenadaPrincipalLat());
							coordenada.setLon(provinciaActual.getCoordenadaPrincipalLon());
							this.markerActual=new MapMarkerDot(provincia,coordenada);
													
							if(this.coordenadas.contains(markerActual.getCoordinate())) {		
								
								markerActual.getStyle().setColor(Color.red);
								markerActual.getStyle().setBackColor(Color.red);
								
								mapa.addMapMarker(markerActual);
								
								this.provinciaOrigen=provinciaActual.getNombre();
								this.hayProvinciaSeleccionada=true;
							}else {
								lanzarAdvertencia("Debe seleccionar un vertice ya existente.");
							}
						}
			}
		}	
		actualizar();				
	}
	private boolean esProvinciaValida(Coordinate punto, Provincia provinciaActual) {
		return punto.getLat()<=provinciaActual.getLimiteLatArriba()
				&& punto.getLat()>=provinciaActual.getLimiteLatAbajo() 
				&& punto.getLon()>=provinciaActual.getLimiteLonIzquierda()
				&& punto.getLon()<=provinciaActual.getLimiteLonDerecha();
	}
	
	private boolean verticesIguales(MapMarker vertice, MapMarker verticeAComparar) {
		
		return vertice.getCoordinate().equals(verticeAComparar.getCoordinate());
	}
	
	private boolean contieneVecino(MapMarker vertice,MapMarker verticeAComparar) {
		if(this.verticesVecinos.get(vertice.getCoordinate())==null) {
			return false;
		}
		
		return this.verticesVecinos.get(vertice.getCoordinate()).contains(verticeAComparar.getCoordinate());
	}
	
	private void agregarVecino( MapMarker vertice,MapMarker verticeVecino) {
		ArrayList<Coordinate> vecinos=this.verticesVecinos.get(vertice.getCoordinate());
	
		if(vecinos==null) { 
			vecinos=new ArrayList<Coordinate>();
			vecinos.add(verticeVecino.getCoordinate());
			this.verticesVecinos.put(vertice.getCoordinate(), vecinos);
			
		}else {
			vecinos=this.verticesVecinos.get(vertice.getCoordinate());
			vecinos.add(verticeVecino.getCoordinate());
			
			this.verticesVecinos.put(vertice.getCoordinate(),vecinos);
			
		}
	}
	
	private void dibujarArista(MapMarker markerNew,String provinciaOrigen,String provinciaDestino,MapMarker markerActual) {
		MapPolygonImpl polygon=new MapPolygonImpl(this.markerActual.getCoordinate(),this.markerActual.getCoordinate(),markerNew.getCoordinate());
		
		polygon.setColor(Color.blue);
		polygon.setBackColor(null);
		this.poligonos.add(polygon);
		
		ArrayList<Coordinate>coordenadasPoligono=new ArrayList<>();
		coordenadasPoligono.add(this.markerActual.getCoordinate());
		coordenadasPoligono.add(markerNew.getCoordinate());
		this.coordenadasPoligonoIndex.put(coordenadasPoligono, this.poligonos.size()-1);
		
		this.mapa.addMapPolygon(polygon);

		this.datosPanel.agregarLblArista(provinciaOrigen,provinciaDestino,markerActual.getCoordinate(),markerNew.getCoordinate());
		this.provinciaOrigen="";
		this.provinciaDestino="";
	}
	
	public void redibujarAristas(HashMap<ArrayList<Integer>, Integer> aristas) {
		aristas.forEach(
				(clave,valor)->redibujarArista(clave)
							
				);		
	}
	
	private void redibujarArista(ArrayList<Integer> vertices) {
		Coordinate origen=this.datosPanel.dameCoordenadaIndex(vertices.get(0));
		Coordinate destino=this.datosPanel.dameCoordenadaIndex(vertices.get(1));
			
		ArrayList<Coordinate> coordenadas=new ArrayList<>();
		coordenadas.add(origen);
		coordenadas.add(destino);
				
		this.mapa.removeAllMapPolygons();
		
		if(this.coordenadasPoligonoIndex.get(coordenadas)!=null) {			
			eliminarPoligonoArista(this.poligonos.get((int)this.coordenadasPoligonoIndex.get(coordenadas)));
		}				
		actualizarPoligonos();				
	}
		
	private void eliminarPoligonoArista(MapPolygonImpl poligonoEliminar) {
		
		this.poligonosEliminados.add(poligonoEliminar);			
	}
	
	private void lanzarAdvertencia(String mensaje) {
		JOptionPane.showMessageDialog(this,
		 mensaje,
	    "Advertencia",
	    JOptionPane.WARNING_MESSAGE);
	}
	
	public void limpiar() {
		this.mapa.removeAllMapMarkers();
		this.mapa.removeAllMapPolygons();
		this.coordenadas.clear();
		this.markerActual=null;
		this.marcadores.clear();
		this.hayProvinciaSeleccionada=false;
		this.coordenadasPoligonoIndex.clear();
		this.verticesVecinos.clear();
		this.poligonos.clear();
		this.poligonosEliminados.clear();
		this.datosPanel.limpiar();
	}

	public HashMap<Coordinate, ArrayList<Coordinate>> getVerticesVecinos() {
		return verticesVecinos;
	}
		
	public ArrayList<MapMarker> getMarcadores() {
		return marcadores;
	}

	public DatosDeMapaPanel getDatosPanel() {
		return datosPanel;
	}		
}