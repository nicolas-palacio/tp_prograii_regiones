package visual;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;

import java.util.ArrayList;
import java.util.HashMap;

import javax.swing.JButton;
import javax.swing.JFrame;
import controlador.Controlador;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import java.awt.Cursor;
import javax.swing.JTextField;


public class Visual {
	private JFrame frame;
	private MapaPanel mapaPanel;
	private JTextField textFieldCantRegiones;
	private JButton btnGenerarRegiones;
	private JLabel lblAristas;
	private JButton btnAceptar;	
	/**
	 * Launch the application.
	 */
	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Visual window = new Visual();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Visual() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		this.frame = new JFrame();
		this.frame.setBounds(100, 100, 950, 950);
		this.frame.setTitle("Creador de Grafos");
		this.frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.frame.setLocationRelativeTo(null);
		this.frame.setResizable(false);

		this.mapaPanel= new MapaPanel();
		mapaPanel.getDatosPanel().setBounds(450, 47, 440, 521);
		this.mapaPanel.setLayout(null);	
		frame.getContentPane().add(mapaPanel);
		
		this.lblAristas = new JLabel("Aristas");
		this.lblAristas.setFont(new Font("Tahoma", Font.ITALIC, 25));
		this.lblAristas.setBounds(508, 21, 106, 25);
		this.mapaPanel.add(lblAristas);
		
		textFieldCantRegiones = new JTextField();
		textFieldCantRegiones.setEnabled(false);
		textFieldCantRegiones.setBounds(640, 746, 66, 20);
		mapaPanel.add(textFieldCantRegiones);
		textFieldCantRegiones.setColumns(10);
		
		JLabel lblCantRegiones = new JLabel("Cantidad de regiones a generar");
		lblCantRegiones.setFont(new Font("Tahoma", Font.ITALIC, 25));
		lblCantRegiones.setBounds(450, 699, 393, 36);
		mapaPanel.add(lblCantRegiones);
		
		    btnAceptar = new JButton("Generar Arbol generador minimo");
			btnAceptar.setFocusPainted(false);
			mapaPanel.add(btnAceptar);
			btnAceptar.setBackground(new Color(255, 255, 255));
			btnAceptar.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
			
			btnAceptar.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseClicked(MouseEvent e) {		
					ejecutarAlgoritmo();
				}
			});
			
			btnAceptar.setFont(new Font("Tahoma", Font.PLAIN, 14));
			btnAceptar.setBounds(546, 617, 249, 57);
			
			this.btnGenerarRegiones = new JButton("Generar Regiones");
			btnGenerarRegiones.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseClicked(MouseEvent e) {
					ejecutarRegiones();
				}
			});
			btnGenerarRegiones.setEnabled(false);
			btnGenerarRegiones.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
			btnGenerarRegiones.setFont(new Font("Tahoma", Font.PLAIN, 14));
			btnGenerarRegiones.setBackground(Color.WHITE);
			btnGenerarRegiones.setBounds(594, 794, 159, 57);
			mapaPanel.add(btnGenerarRegiones);			
			
			JLabel lblPesos = new JLabel("Pesos");
			lblPesos.setFont(new Font("Tahoma", Font.ITALIC, 25));
			lblPesos.setBounds(808, 21, 82, 25);
			mapaPanel.add(lblPesos);
			
			JButton btnLimpiar = new JButton("Limpiar");
			btnLimpiar.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseClicked(MouseEvent e) {
					limpiarMapa();
				}
			});
			btnLimpiar.setFont(new Font("Tahoma", Font.PLAIN, 14));
			btnLimpiar.setBackground(Color.WHITE);
			btnLimpiar.setBounds(834, 864, 87, 46);
			mapaPanel.add(btnLimpiar);
	}
			
	private void ejecutarAlgoritmo() {
		if(puedeEjecutar()) {		
			
			mapaPanel.getDatosPanel().generarPesos(mapaPanel.getVerticesVecinos());
			mapaPanel.getDatosPanel().generarCoordenadaVertice(mapaPanel.getMarcadores());
			int cantVertices=this.mapaPanel.getMarcadores().size();
			HashMap<ArrayList<Integer>, Integer> aristasNuevas=Controlador.generarAGM(cantVertices,this.mapaPanel.getDatosPanel().getVerticesConAristas());	
			
			this.mapaPanel.redibujarAristas(aristasNuevas);
			habilitarBotonRegion(true);		
			this.btnAceptar.setEnabled(false);
		}
	}
	
	private boolean puedeEjecutar() {
		if(this.mapaPanel.getMarcadores().isEmpty()) {
			lanzarAdvertencia("El mapa no posee vertices ni aristas.");
			return false;
		}if(!pesosValidos()) {
			lanzarAdvertencia("Ingrese un peso valido. Pesos validos: mayores a 0 y menores o iguales a 999");
			return false;
		}		
		else {
			return true;
		}
	}
			
	private boolean pesosValidos() {
		ArrayList<JTextField> listaPesos=this.mapaPanel.getDatosPanel().getTextFieldLista();
		for(int i=0;i<listaPesos.size();i++) {
			if(listaPesos.get(i).getText().equals("")) {
				lanzarAdvertencia("Introduzca el peso de todas las aristas.");
				return false;
			}
			int pesoActual=Integer.parseInt(listaPesos.get(i).getText());
			if(pesoActual<=0 || pesoActual>999) {
				return false;
			}		
		}		
		return true;
	}
		
	private void habilitarBotonRegion(boolean opcion) {

		this.btnGenerarRegiones.setEnabled(opcion);
		this.textFieldCantRegiones.setEnabled(opcion);
	}
	
	private void ejecutarRegiones() {
		if(puedeGenerarRegiones()) {		
			HashMap<ArrayList<Integer>, Integer> aristasNuevas=Controlador.generarRegiones(Integer.parseInt(this.textFieldCantRegiones.getText()));
			this.mapaPanel.redibujarAristas(aristasNuevas);
			habilitarBotonRegion(false);
		}	
	}
	
	private boolean puedeGenerarRegiones() {
		int cantidadRegiones=Integer.parseInt(this.textFieldCantRegiones.getText());
		
		if(cantidadRegiones>this.mapaPanel.getMarcadores().size()) {
			lanzarAdvertencia("La cantidad de regiones no puede ser mayor que la cantidad de vertices");
			return false;
		}		
		return true;
	}
	
	private void lanzarAdvertencia(String mensaje) {
		JOptionPane.showMessageDialog(this.frame,
		 mensaje,
	    "Advertencia",
	    JOptionPane.WARNING_MESSAGE);
	}
	
	private void limpiarMapa() {
		this.textFieldCantRegiones.setText("");
		habilitarBotonRegion(false);
		this.btnAceptar.setEnabled(true);
		this.mapaPanel.limpiar();					
	}
}	