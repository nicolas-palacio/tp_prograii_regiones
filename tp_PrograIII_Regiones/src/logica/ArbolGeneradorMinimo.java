package logica;

import java.util.ArrayList;

public class ArbolGeneradorMinimo extends Grafo{
	private  ArrayList<Integer> verticesVisitados;
	
	public ArbolGeneradorMinimo(Grafo grafo,int vertices) {
		super(vertices);

		this.verticesVisitados=new ArrayList<Integer>();

		generarArbol(grafo);
	}
		
	private void generarArbol(Grafo grafo) {
		if(grafo==null) {
			throw new IllegalArgumentException("El grafo no puede ser null");
		}
			
		boolean[] verticesMarcados=new boolean[grafo.cantidadVertices()];
		verticesMarcados[0]=true;
		
		int i=0;
		while(i<grafo.cantidadVertices()-1) {		
				int[] verticeNuevo=aristaMenorPeso(grafo.getPesos(),verticesParaRecorrer(grafo,verticesMarcados));	
				super.agregarArista(verticeNuevo[0], verticeNuevo[1], verticeNuevo[2]);
				verticesMarcados[verticeNuevo[1]]=true;

				i++;
		}
	
	}
	
	private static int[] aristaMenorPeso(int[][] pesos,ArrayList<Integer> verticesParaRecorrer) {
		int arista=100000;
		int[] vertice=new int[3];//[0] vertice actual, [1] vertice destino, [2] peso

		int i=0,j=0;
		while(i<verticesParaRecorrer.size()) {			
			int filaActual=verticesParaRecorrer.get(i);	

			if(verticesParaRecorrer.contains(j)) {//no chequea un vertice/arista que ya posee
				j++;

			}else {					
				if (j >= pesos[0].length) {
					i++;
					j = 0;
				}else if(pesos[filaActual][j]<arista  && pesos[filaActual][j]!=0 ) { 
					
					arista=pesos[filaActual][j];
					vertice[0]=filaActual;
					vertice[1]=j;
					vertice[2]=arista;//peso arista		
				} else {
					j++;
				}
			}
		}		
		return vertice;
	}
	
	private static ArrayList<Integer> verticesParaRecorrer(Grafo grafo,boolean[] verticesMarcados) {
		ArrayList<Integer> ret=new ArrayList<>();
		
		for(int i=0;i<verticesMarcados.length;i++) {
			if(verticesMarcados[i]) {
				ret.add(i);
			}
		}
		return ret;		
	}
	
	public void generarRegiones(int cantidadRegiones) {
		if(cantidadRegiones>super.cantidadVertices()) {
			throw new IllegalArgumentException("La cantidad de regiones no puede ser mayor que la cantidad de vertices");
		}
		
		ArbolGeneradorMinimo generarUnaRegion=new ArbolGeneradorMinimo(this,super.cantidadVertices());
		generarUnaRegion.setCantidadRegiones(cantidadRegiones);
		super.setCantidadRegiones(cantidadRegiones);
		
		int i=0;
		while(i<cantidadRegiones-1) {
	
			generarUnaRegion(this);
			i++;
		}
				
	}
	
	private void generarUnaRegion(ArbolGeneradorMinimo grafo) {	
		int aristaMayorActual=0;
		int[] vertice=new int[2];// [0] origen [1] destino
			
		int fila=0,columna=0;
		  while (fila < grafo.getPesos().length) {		  	 
	            if (columna ==  grafo.getPesos()[0].length - 1) {
	            	fila++;
	                columna = 0;
	            }else if(grafo.getPesos()[fila][columna]>aristaMayorActual) {
	            	aristaMayorActual=grafo.getPesos()[fila][columna];
	            	vertice[0]=fila ;
	            	vertice[1]=columna ;
	            	columna++;
	            }
	            else {
	            	columna++;
	            }	 
	        }		
		this.borrarArista(vertice[0], vertice[1]);				
	}

	public String mostrarRegiones(Grafo grafo) {
		 String regiones="";
			
		int vertice=0,numeroRegion=0;
		while(vertice<grafo.cantidadVertices()) {
			if(!this.verticesVisitados.contains(vertice)) {
				
				regiones+="Region: "+(numeroRegion+1);
				regiones+=encontrarUnaRegion(grafo,vertice);
				regiones+="\n";
				numeroRegion++;
			}
			vertice++;
		}
				
		return regiones;
	}	
	
	private String encontrarUnaRegion(Grafo grafo,int verticeActual) {
		String region="";	
		ArrayList<Integer> verticesPorVisitar=grafo.getVecinos().get(verticeActual);
		
		int fila=verticeActual,columna=0;
		while(fila<grafo.getA().length) {
			if(verticesPorVisitar.isEmpty()){
				region+=" Vertice: "+verticeActual;
				this.verticesVisitados.add(verticeActual);
				break;
			}
			else if(verticesPorVisitar.contains(fila) || fila==verticeActual) {
				if (columna >=  grafo.getA()[0].length) { 
		            	fila++;
		                columna = 0;            
	            }else if(grafo.getA()[fila][columna]) {
	            		region+=" Vertice: "+fila+"---"+columna;

	            		verticesPorVisitar.addAll(grafo.getVecinos().get(columna));            		
	            		this.verticesVisitados.add(columna);
	            		this.verticesVisitados.add(fila);
	            		columna ++;
	            }
			 else {
	            	columna ++;
	            }
			}
			else {
				fila++;
			}
		}				
		return region;
	}			
}