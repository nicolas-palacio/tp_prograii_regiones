package logica;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

public class Grafo {
	//Matriz de adyacencia
	private boolean[][] A; 
	private int[][] pesos;
	private HashMap<Integer,ArrayList<Integer>> vecinos;
	private int vertices,cantidadRegiones;
	
	public Grafo(int vertices) {
		this.A=new boolean[vertices][vertices];
		this.pesos=new int[vertices][vertices];
		this.vertices=vertices;
		this.cantidadRegiones=-1;
		this.vecinos=new HashMap<Integer,ArrayList<Integer>>(); 
	}
			

	public boolean[][] getA() {
		return A;
	}

	public int[][] getPesos() {
		return pesos;
	}

	public void agregarArista(int i, int j,int pesoArista) {
		verificarVertices(i,j);
		verificarPeso(pesoArista);	
		
		A[i][j]=true;
		A[j][i]=true;
		pesos[i][j]=pesoArista;
		pesos[j][i]=pesoArista;
				
	}
	
	public void borrarArista(int i,int j) {
		verificarVertices(i,j);
	
		A[i][j]=false;
		A[j][i]=false;
		pesos[i][j]=0;
		pesos[j][i]=0;
	}
		
	public boolean existeArista(int i,int j) {
		verificarVertices(i,j);
		
		return A[i][j];
	}
	
	private void verificarVertices(int i,int j) {
		if(i<0 || j<0) {
			throw new IllegalArgumentException("El vertice no puede ser negativo");
		}
		if(i>=this.cantidadVertices() || j>=this.cantidadVertices()) {
			throw new IllegalArgumentException("El vertice no puede ser mayor que la cantidad de vertices del grafo");
		}
		if(i==j) {
			throw new IllegalArgumentException("Los vertices no pueden ser iguales: "+i+"="+j);
		}
	}
	
	private void verificarPeso(int peso) {
		if(peso<=0) {
			throw new IllegalArgumentException("El peso debe ser mayor a cero: "+peso);
		}
	}
		
	public int cantidadVertices() {
		return this.vertices;
	}
	
	public void setCantidadRegiones(int cantidad) {
		this.cantidadRegiones=cantidad;
	}
	
	
	public int getCantidadRegiones() {
		return cantidadRegiones;
	}
		
	public HashMap<Integer, ArrayList<Integer>> getVecinos() {
		if(this.vecinos.isEmpty()) {
			obtenerTodosLosVecinos();
		}
		return this.vecinos;
	}



	public void obtenerTodosLosVecinos() {				
		for(int fila=0;fila<this.vertices;fila++) {
			ArrayList<Integer> vecinosVertice=new ArrayList<>();
			
			for(int columna=0;columna<this.getA()[0].length;columna++) {
				if(this.getA()[fila][columna]) {
					vecinosVertice.add(columna);
				}				
			}
			this.vecinos.put(fila, vecinosVertice);
					
		}

	}

	@Override
	public  String toString() {
		String ret="";
		for(int i=0;i<this.A.length;i++) { //fila
			ret+="estoy en vertice numero: "+i+"\n";
			for(int j=0;j<this.A[0].length;j++) {//columna
				ret+="arista: "+i+"--"+j+" con peso:"+this.pesos[i][j]+"\n";
			}
			ret+="\n";
		}
		return ret;
	}
		
}

