package logica;




public class Provincia {
	private String nombre;
	private double coordenadaPrincipalLat,coordenadaPrincipalLon;
	private double limiteLatArriba,limiteLatAbajo,limiteLonIzquierda,limiteLonDerecha;
	
	public Provincia(String nombre,double coordenadaPrincipalLat,double coordenadaPrincipalLon,
			double limiteLatArriba, double limiteLatAbajo,double limiteLonIzquierda,
			double limiteLonDerecha	) {
				
				this.nombre=nombre;
				this.coordenadaPrincipalLat=coordenadaPrincipalLat;
				this.coordenadaPrincipalLon=coordenadaPrincipalLon;
				this.limiteLatArriba=limiteLatArriba;
				this.limiteLatAbajo=limiteLatAbajo;
				this.limiteLonIzquierda=limiteLonIzquierda;
				this.limiteLonDerecha=limiteLonDerecha;		
				
	}

	public String getNombre() {
		return nombre;
	}

	public double getCoordenadaPrincipalLat() {
		return coordenadaPrincipalLat;
	}

	public double getCoordenadaPrincipalLon() {
		return coordenadaPrincipalLon;
	}

	public double getLimiteLatArriba() {
		return limiteLatArriba;
	}

	public double getLimiteLatAbajo() {
		return limiteLatAbajo;
	}

	public double getLimiteLonIzquierda() {
		return limiteLonIzquierda;
	}

	public double getLimiteLonDerecha() {
		return limiteLonDerecha;
	}
	
	
}
