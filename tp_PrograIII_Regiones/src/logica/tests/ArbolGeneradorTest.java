package logica.tests;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import logica.ArbolGeneradorMinimo;
import logica.Grafo;



public class ArbolGeneradorTest {
	private ArbolGeneradorMinimo arbolGeneradorMinimo;
	@Before
	public void setup() {
		Grafo grafo=new Grafo(5);
		grafo.agregarArista(0, 1, 2);
		grafo.agregarArista(0, 2, 8);
		grafo.agregarArista(1, 2, 5);
		grafo.agregarArista(1, 3, 5);
		grafo.agregarArista(2, 4, 11);
		grafo.agregarArista(3, 4, 1);
		
		this.arbolGeneradorMinimo=new ArbolGeneradorMinimo(grafo,grafo.cantidadVertices());
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void arbolNullTest() {
		ArbolGeneradorMinimo aGM=new ArbolGeneradorMinimo(null,0);
		
	}

	
	@Test
	public void arbolEsperadoTest() {
		Grafo arbolEsperado=new Grafo(5);
		arbolEsperado.agregarArista(0, 1, 2);
		arbolEsperado.agregarArista(1, 2, 5);
		arbolEsperado.agregarArista(1, 3, 5);
		arbolEsperado.agregarArista(3, 4, 1);
		
		Assert.arbolesIguales(this.arbolGeneradorMinimo,arbolEsperado);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void regionesMayoresQueVerticesTest() {
		this.arbolGeneradorMinimo.generarRegiones(this.arbolGeneradorMinimo.cantidadVertices()+1);	
	}
	
}
