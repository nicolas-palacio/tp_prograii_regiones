package logica.tests;

import static org.junit.Assert.*;

import java.util.HashSet;

import org.junit.Test;

import logica.Grafo;


public class GrafoTest {

	@Test (expected=IllegalArgumentException.class)
	public void agregarAristaVerticeIgual()
	{
		Grafo g = new Grafo(5);
		g.agregarArista(0,0,1);
	}

	@Test (expected=IllegalArgumentException.class)
	public void agregarAristaFueraDeRangoSuperior()
	{
		Grafo g = new Grafo(5);
		g.agregarArista(5,0,1);
	}

	
	@Test (expected=IllegalArgumentException.class)
	public void agregarAristaNegativa()
	{
		Grafo g = new Grafo(5);
		g.agregarArista(-1,0,1);
	}


	@Test
	public void agregarAristaValidaTest() 
	{
		Grafo g = new Grafo(5); 
		g.agregarArista(0, 1,1); 
		assertTrue(g.existeArista(0, 1)); 
	}
	
	@Test
	public void agregarAristaSimetriacaTest() 
	{
		Grafo g = new Grafo(5); 
		g.agregarArista(0, 1,1); 
		assertTrue(g.existeArista(1, 0)); 
	}
	
	@Test
	public void eliminarAristaTest() 
	{
		Grafo g = new Grafo(5);
		g.agregarArista(0, 1,1);
		g.borrarArista(0, 1);
		assertFalse(g.existeArista(0, 1));
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void pesoInvalidoTest() 
	{
		Grafo g = new Grafo(5);
		g.agregarArista(0, 1,0);

	
	}
	


}
