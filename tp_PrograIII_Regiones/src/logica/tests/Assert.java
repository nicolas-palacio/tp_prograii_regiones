package logica.tests;

import static org.junit.Assert.assertEquals;

import logica.Grafo;

public class Assert {

	public static void arbolesIguales(Grafo grafoOriginal,Grafo grafoAComparar) {
		
		assertEquals(grafoAComparar.toString(),grafoOriginal.toString());
		
		assertEquals(grafoAComparar.cantidadVertices(),grafoOriginal.cantidadVertices());
		
	}
}
