package logica;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;

import org.openstreetmap.gui.jmapviewer.Coordinate;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;



public class ProvinciaJSON {
	
	private ArrayList<Provincia> provincias;
	
	public ProvinciaJSON() {
		this.provincias=new ArrayList<>();
	}
				
	public ArrayList<Provincia> getProvincias() {
		return provincias;
	}
	

	public static ProvinciaJSON leerJSON(String archivo) {
		Gson gson= new Gson();
		ProvinciaJSON ret=null;
		
		try {
			BufferedReader br= new BufferedReader(new FileReader(archivo));
			ret=gson.fromJson(br, ProvinciaJSON.class);
		}catch(IOException e) {
			e.printStackTrace();
		}
		
		return ret;
	}
	

}
